package com.marion.micro.app.common.executor;

import lombok.Data;

@Data
public abstract class AbstractThreadPoolProperties {

    private int corePoolSize;
    private int maxPoolSize;
    private int keepAliveSeconds;
    private int queueCapacity;
    private String threadNamePrefix;

}
