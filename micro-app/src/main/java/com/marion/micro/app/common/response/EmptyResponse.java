package com.marion.micro.app.common.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmptyResponse {

    @Override
    public String toString() {
        return "{}";
    }

}
