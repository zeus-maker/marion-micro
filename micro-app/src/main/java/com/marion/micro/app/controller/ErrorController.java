package com.marion.micro.app.controller;

import com.marion.micro.app.common.response.AppError;
import com.marion.micro.app.common.response.ResponseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/error")
public class ErrorController {

    @Value("${spring.application.name}")
    private String applicationName;

    @GetMapping("/case1")
    public Object case1(@RequestParam(required = false) long id) {
        if (id == 1) {
            throw new NullPointerException();
        }
        return applicationName + " case34";
    }

    @GetMapping("/case2")
    public Object case2(@RequestParam(required = false) long id) {
        if (id == 1) {
            throw new ResponseException(AppError.E_PARAM);
        }
        return applicationName + " case2";
    }

}
