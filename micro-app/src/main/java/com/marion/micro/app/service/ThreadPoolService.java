package com.marion.micro.app.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ThreadPoolService {

    @Async(value = "kafkaThreadPool")
    public void produce() {
        log.info("current thread pool name={}", Thread.currentThread().getName());
    }

}
