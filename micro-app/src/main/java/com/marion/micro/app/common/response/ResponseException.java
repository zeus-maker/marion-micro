package com.marion.micro.app.common.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ResponseException extends RuntimeException {

    private AppError error;

    public ResponseException(AppError error) {
        super(error.getMessage());
        this.error = error;
    }

}
