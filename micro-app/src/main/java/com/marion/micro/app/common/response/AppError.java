package com.marion.micro.app.common.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AppError {
    /**
     * 错误类
     */
    E_SERVER(10000, "服务器异常"),
    E_PARAM(10001, "参数错误"),
    ;

    private int code;
    private String message;

}
