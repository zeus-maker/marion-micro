package com.marion.micro.app.common.executor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "thread-pool.biz-consumer")
public class BizThreadPoolConf extends AbstractThreadPoolProperties  {

}
