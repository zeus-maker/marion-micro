package com.marion.micro.app.common.executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * 线程池配置，线程池资源隔离
 * 1. Kafka消费线程池
 * 2. 业务线程池
 */
@Configuration
public class ThreadPoolConfig {

    @Autowired
    private KafkaThreadPoolConf kafkaThreadPoolConf;

    @Autowired
    private BizThreadPoolConf bizThreadPoolConf;

    @Bean("kafkaThreadPool")
    public Executor kafkaThreadPool() {
        return getThreadPoolTaskExecutor(kafkaThreadPoolConf);
    }

    @Bean("bizThreadPool")
    public Executor bizThreadPool() {
        return getThreadPoolTaskExecutor(bizThreadPoolConf);
    }

    private Executor getThreadPoolTaskExecutor(AbstractThreadPoolProperties threadPool) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(threadPool.getCorePoolSize());
        executor.setMaxPoolSize(threadPool.getMaxPoolSize());
        executor.setQueueCapacity(threadPool.getQueueCapacity());
        executor.setThreadNamePrefix(threadPool.getThreadNamePrefix());
        executor.initialize();
        return executor;
    }

}
