package com.marion.micro.app.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class IndexController {

    @Value("${spring.application.name}")
    private String applicationName;

    @GetMapping("/")
    public Object admin() {
        return applicationName + "-admin";
    }

}
