package com.marion.micro.app.common.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseResult {

    private int code;

    private String message;

    private Object data = new EmptyResponse();

}
