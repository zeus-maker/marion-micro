package com.marion.micro.app.service.cache;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CacheTestService {

    @Autowired
    private RedissonClient redissonClient;

    public void testRedis() {
        System.out.println(redissonClient);
    }

    /**
     * 1. 测试分布式锁（阻塞）
     */
    public void testDistributeLock(int i) {
        String key = "testDistributeLock";
        RLock lock = redissonClient.getLock(key);
        lock.lock();
        try {
            System.out.println("get lock succeed. i=" + i);
            Thread.sleep(3000L);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    /**
     * 2. 测试分布式锁（非阻塞）
     */
    public void testDistributeLock002(int i) {
        String key = "testDistributeLock";
        RLock lock = redissonClient.getLock(key);
        boolean tryLock = lock.tryLock();
        if (tryLock) {
            try {
                System.out.println("get lock succeed. i=" + i);
                Thread.sleep(3000L);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        } else {
            System.out.println("get lock failed. i=" + i);
        }
    }


    /**
     * 3. 测试分布式锁（非阻塞）
     */
    public void testDistributeLock003(int i) {
        String key = "testDistributeLock";
        RLock lock = redissonClient.getLock(key);
        boolean tryLock;
        try {
            tryLock = lock.tryLock(1, TimeUnit.SECONDS);
            if (tryLock) {
                System.out.println("get lock succeed. i=" + i);
                Thread.sleep(3000L);
            } else {
                System.out.println("get lock failed. i=" + i);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
