package com.marion.micro.app.common.executor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "thread-pool.kafka-consumer")
public class KafkaThreadPoolConf extends AbstractThreadPoolProperties  {

}
