package com.marion.micro.app.controller;

import com.marion.micro.app.common.response.AppError;
import com.marion.micro.app.common.response.ResponseException;
import com.marion.micro.app.service.ThreadPoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pool")
public class ThreadPoolController {

    @Autowired
    private ThreadPoolService threadPoolService;

    @Value("${spring.application.name}")
    private String applicationName;

    @GetMapping("/case1")
    public Object case1() {
        threadPoolService.produce();
        return null;
    }

    @GetMapping("/case2")
    public Object case2(@RequestParam(required = false) long id) {
        if (id == 1) {
            throw new ResponseException(AppError.E_PARAM);
        }
        return applicationName + " case2";
    }

}
