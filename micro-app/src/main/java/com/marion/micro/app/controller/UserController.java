package com.marion.micro.app.controller;

import com.marion.micro.common.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "用户测试类")
@RestController
@RequestMapping("/")
public class UserController {

    @Value("${spring.application.name}")
    private String applicationName;

    @ApiOperation(value = "管理", notes = "管理")
    @GetMapping("/admin")
    public Object admin() {
        return applicationName + "admin";
    }

    @GetMapping("/admin/test")
    public Object adminTest() {
        return applicationName + "admin test";
    }

    @GetMapping("/user")
    public UserVO user() {
        return UserVO.builder()
                .id(1)
                .name("marion")
                .build();
    }

    @GetMapping("/test")
    public Object hello(@RequestParam(required = false) long id) {
        if (id == 1) {
            throw new NullPointerException();
        }
        return applicationName + "test";
    }

}
