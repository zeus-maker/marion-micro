package com.marion.micro.app.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ErrorControllerTest extends ControllerBaseTest {

    public static final String DOMAIN = "http://localhost:10020";

    @DisplayName("测试空指针异常返回")
    @Test
    void case001() throws Exception {
        String url = DOMAIN + "/error/case1?id=1";
        apiTest(url);
    }

    @DisplayName("测试自定义异常返回")
    @Test
    void case002() throws Exception {
        String url = DOMAIN + "/error/case2?id=1";
        apiTest(url);
    }
}