package com.marion.micro.app.service.cache;

import com.marion.micro.app.controller.ControllerBaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CacheTestServiceTest extends ControllerBaseTest {

    @Autowired
    private CacheTestService cacheTestServiceUnderTest;

    @Test
    public void testTestRedis() {
        // Setup
        // Run the test
        cacheTestServiceUnderTest.testRedis();

        // Verify the results
    }

    @Test
    public void testTestDistributeLock001() {
        // Setup
        // Run the test
        cacheTestServiceUnderTest.testDistributeLock(1);

        // Verify the results
    }

    @Test
    public void testTestDistributeLock002() {
        // Setup
        // Run the test
        for (int i = 0; i < 5; i++) {
            cacheTestServiceUnderTest.testDistributeLock(i);
        }

        // Verify the results
    }

    /**
     * 测试可重入锁
     */
    @Test
    public void testTestDistributeLock003() {
        // Setup
        // Run the test
        for (int i = 0; i < 5; i++) {
            cacheTestServiceUnderTest.testDistributeLock002(i);
        }

        // Verify the results
    }

    /**
     * 测试多线程获取锁
     */
    @Test
    public void testTestDistributeLock004() {
        // Setup
        // Run the test
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            int finalI = i;
            executorService.submit(() -> cacheTestServiceUnderTest.testDistributeLock002(finalI));
        }

        // Verify the results
    }
}
