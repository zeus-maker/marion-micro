package com.marion.micro.common.feign;

import org.springframework.web.bind.annotation.GetMapping;

public interface MicroWebService {

    @GetMapping("/user")
    String user();

}
