package com.marion.micro.common.feign;

import org.springframework.web.bind.annotation.GetMapping;

public interface MicroAppService {

    @GetMapping("/user")
    String user();

}
