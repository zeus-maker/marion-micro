package com.marion.micro.common.feign;

import com.marion.micro.common.vo.UserVO;
import org.springframework.web.bind.annotation.GetMapping;

public interface MicroAdminService {

    @GetMapping("/user")
    String user();

    @GetMapping("/user/vo")
    UserVO userVo();

}
