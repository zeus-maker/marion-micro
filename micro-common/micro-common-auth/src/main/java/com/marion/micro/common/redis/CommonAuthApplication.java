package com.marion.micro.common.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class CommonAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonAuthApplication.class, args);
    }

}
