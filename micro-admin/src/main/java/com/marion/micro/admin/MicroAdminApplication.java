package com.marion.micro.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MicroAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroAdminApplication.class, args);
    }

}
