package com.marion.micro.admin.controller;

import com.marion.micro.common.vo.UserVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class UserController {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    @GetMapping("/admin")
    public Object admin() {
        return applicationName + port +  "admin";
    }

    @GetMapping("/admin/test")
    public Object adminTest() {
        return applicationName + port + " admin test";
    }

    /**
     * 测试服务注册发现
     */
    @GetMapping("/user")
    public Object user() {
        try {
            Thread.sleep(10000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return applicationName + port + "user";
    }

    @GetMapping("/test")
    public Object hello() {
        return applicationName + port + "test";
    }

    @GetMapping("/user/vo")
    public UserVO userVo() {
        return UserVO.builder()
                .id(1)
                .name("marion")
                .build();
    }
}
