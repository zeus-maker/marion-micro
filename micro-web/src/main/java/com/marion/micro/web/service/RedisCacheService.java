package com.marion.micro.web.service;

public interface RedisCacheService {

    void setHello(String key, String value);

    String getHello(String key);

}
