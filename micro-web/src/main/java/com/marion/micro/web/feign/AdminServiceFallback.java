package com.marion.micro.web.feign;

import com.marion.micro.common.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component("AdminServiceFallback")
public class AdminServiceFallback implements AdminService {

    @Override
    public String user() {
        return "fallback";
    }

    @Override
    public UserVO userVo() {
        return UserVO.builder().id(0).build();
    }
}