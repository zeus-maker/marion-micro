package com.marion.micro.web.controller;

import com.marion.micro.common.vo.UserVO;
import com.marion.micro.web.feign.AdminService;
import com.marion.micro.web.service.RedisCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class UserController {

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private RedisCacheService redisCacheService;

    @Autowired
    private AdminService adminService;

    @GetMapping("/admin")
    public Object admin() {
        return applicationName + "admin";
    }

    @GetMapping("/admin/test")
    public Object adminTest() {
        return applicationName + "admin test";
    }

    @GetMapping("/user")
    public Object user() {
        return applicationName + "user";
    }

    @GetMapping("/test")
    public Object hello() {
        return applicationName + "test";
    }

    /**
     * 测试返回String
     */
    @GetMapping("/rpc/user")
    public Object rpcUser() {
        return adminService.user();
    }

    /**
     * 测试返回VO
     */
    @GetMapping("/rpc/user/vo")
    public UserVO rpcUserVo() {
        return adminService.userVo();
    }

    /**
     * 测试Redis
     */
    @GetMapping("/redis/set")
    public Object rpcUserVo(@RequestParam String key,
                            @RequestParam String val) {
        redisCacheService.setHello(key, val);
        return "ok";
    }

    @GetMapping("/redis/get")
    public Object rpcUserVo(@RequestParam String key) {
        return redisCacheService.getHello(key);
    }

}
