package com.marion.micro.web.feign;

import com.marion.micro.common.vo.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component("adminService")
@FeignClient(value = "marion-micro-admin", path = "/", fallback = AdminServiceFallback.class)
public interface AdminService {

    @GetMapping("/user")
    String user();

    @GetMapping("/user/vo")
    UserVO userVo();
}
