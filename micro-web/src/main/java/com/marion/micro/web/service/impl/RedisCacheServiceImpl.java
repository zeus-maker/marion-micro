package com.marion.micro.web.service.impl;

import com.marion.micro.web.service.RedisCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisCacheServiceImpl implements RedisCacheService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void setHello(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, 1, TimeUnit.MINUTES);
    }

    @Override
    public String getHello(String key) {
        return redisTemplate.opsForValue().get(key);
    }
}
